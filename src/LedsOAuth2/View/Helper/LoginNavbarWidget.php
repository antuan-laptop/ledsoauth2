<?php
namespace LedsOAuth2\View\Helper;

use Zend\View\Helper\AbstractHtmlElement;

class LoginNavbarWidget extends AbstractHtmlElement{

    protected $options = array(
        'image_attributes' => array(
            'src', 'alt', 'class'
        ),
        // 'link_attributes' => array(),
        // 'mail_attributes' => array(),
    );

    public function __invoke($options = array()){
        $this->options = array_merge_recursive($this->options, $options);
		return $this;
    }

    public function openTag(){
        return $this->getView()->headLink()
            ->appendStylesheet($this->getView()->basePath('/css/cssBox.css')); 
        //return '<pre>' . PHP_EOL;
    }

    public function getBox(){
        $markup = '';
        $imgAttribs['src'] = $_SESSION['auth']['imageUrl'];
        $imgAttribs['alt'] = $_SESSION['auth']['name'] . 'image';
        $imgAttribs['class'] = 'imageUrl';
        $imgAttribs = array_merge($imgAttribs, $this->options['image_attributes']);

        $format = '<span>Welcome <span style="color: #FFDCA8">' . $_SESSION['auth']['name'] . '</span>&nbsp;&nbsp;&nbsp;';
        $format .= '<img%s><b class="caret"></b>';
        $args = array(
            $this->htmlAttribs($imgAttribs)
        );
        $markup .= vsprintf($format, $args) . PHP_EOL;
        return $markup;
        // print_r($_SESSION['auth']);
    }

    public function closeTag(){
        return '</span>' . PHP_EOL;
    }

    public function __toString(){
        return $this->openTag() . $this->getBox() . $this->closeTag();
    }

}
